
#' Function that performs iterative gradient boosting using convex loss function (e.g., AdaBoost, LogLossBoost aka LogitBoost, etc)
#' 
#' @param x matrix or data frame with values for examples (rows) and features (columns)
#' @param y numeric labels as -1, +1
#' @param niter number of iterations
#' @param method boosting method, can be 'ada' or 'logloss'
#' @param base.classifier A function that can take in x, y, and weight vector; returns labels -1, +1 
#' 
#' @return Boosting model of class 'boost'
#' 
adaboost <- function(x, y, niter, method="ada", base.classifier=tree.base.classifier, factors=NULL, verbose=F, ...) {
  if (!is.data.frame(x)) x <- as.data.frame(x)
  method.options <- c("ada", "logloss")
  method <- method.options[pmatch(method, method.options)]
  if (is.na(method)) { stop(sprintf("Must specific valid method.  Options: %s", paste(method.options, collapse=", "))); }
  check.boost.params(x,y,factors)

  n <- nrow(x)  ## number of examples
  f <- rep(0,n)  ## The predictions on training set
  p <- rep(1/2,n) ## p in logitboost

  # a list to save model variables
  model = list()
  model$iters <- list()
  model$method <- method
  if (method=="ada") {
    wp.func <- function(m,params){w <- exp(-m) + params$epsilon; return(list(w=w))}
  } else if (method=="logloss") { 
    wp.func <- function(m,params){w <- exp(-m) / (1+exp(-m)) + params$epsilon; return(list(w=w))}
  }
  model$weight.pot.func <- wp.func


  # The minimum value for weight
  epsilon = 1e-15
  params <- list(epsilon=epsilon)

  ## Format x as a data.frame
  d <- as.data.frame(x)
  if (!is.null(factors)) {  for (j in 1:dim(x)[2]) {    levels(d[[j]]) <- factors[[j]];     };    }

  x <- as.data.frame(x)
  ## Aggregate weak learners
  for (i in 1:niter) { 
    if (i %% 100 == 0) { gc(); }


    # The different weighting methods for different boosters
    if (method %in% c("ada", "logloss", "longservedio")) { 
      r <- wp.func(y*f, params)
      w <- r$w
      w <- w/sum(w)
      tree <- base.classifier(x, y, w, ...)
      pred <- predict(tree, newdata=x)
      ##pred <- (-1)+2*(predict(tree,x,type="prob")[,1] < predict(tree,x,type="prob")[,2])
      ##pred <- as.numeric(colnames(pred)[as.numeric(pred[,2] > pred[,1])+1])
           
    
      err <- sum(w*(1-(y==pred)))
      alpha1 <- 0.5*log ((1-err-epsilon) / (err+epsilon))  ## original adaboost alpha, freund & schapire, 1995
      alpha2 <- 0.5*log(sum(w[y==pred]) / sum(w[y!=pred])) ## confidence rated adaboost, schapire & singer, 1998
      alpha <- alpha2  ## use confidence rated boosting
      gamma <- sum((w/sum(w))*y*pred)
      ##if (verbose) {show(c(err, gamma, alpha1, alpha2))}
      f <- f + alpha*pred
    }

    margins <- f * as.vector(y)
    model$iters[[i]] <- list()
    model$iters[[i]]$tree <- tree
    model$iters[[i]]$alpha <- alpha
    model$iters[[i]]$params <- params

    if (verbose && (i < 10 || (i < 100 && i %% 10 == 0) || (i < 500 && i %% 25 == 0)|| (i < 10000 && i %% 100 == 0))) {
      cat(sprintf("   Iter %d (train.err=%0.5f, gamma=%0.6f, alpha=%0.6f, model.size=%.1fMB)\n", i, mean(margins<0), gamma, alpha, object.size(model)/10^6))
    }
  }

  class(model)  <- 'boost'
  return (model)
}


logitboost <- function(...) { adaboost(..., method="logloss"); }
