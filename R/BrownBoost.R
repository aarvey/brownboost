library(rpart)
#source("AdaBoost.R")


#' RobustBoost
#' An implementation of BrownBoost that increases margin using non-convex optimization.  Original BrownBoost does not effectively increase positive margin.
#' 
#' @param x training data
#' @param y training labels
#' @param base.classifier classification algorithm with class 'base.classifier'.  Several base classifiers are provided as part of this package.  Adding your own classifier can be acchieved by a simple wrapper object (see documentation or tree.base.classifier as an example).
#' @param factors defines which columns of the matrix 'x' should be treated as factors.
#' @param verbose Outputs details on each iteration (optimization constraints, debugging info, etc)
#' @param max.iters Sets maximum number of iterations allowed
#' @param use.conf Use confidence rated boosting
#' @return Model that can be used to predict on unseen data (class: 'booster')
#' 
#'
robustboost <- function(...) driftboost(...)

#' describeIn robustboost BrownBoost algorithm is noise tolerant, doesn't maximize margin
brownboost <- function(..., noise.estimate=NULL, params=NULL) { 
  if (is.null(params) && is.null(noise.estimate)) stop("Must specify 'params' or 'noise.esimate'")
  if (is.null(params)) params <- get.brown.boost.params(noise.estimate=noise.estimate, margin.target=0)
  driftboost(..., params=params)
}

#' describeIn robustboost Is driver algorithm for RobustBoost, BrownBoost.  Named after 'drifting games' of Freund and Opper (2002)
driftboost <- function(x, y, params, base.classifier=tree.base.classifier, factors=NULL, verbose=F, max.iters=Inf, use.conf=F) {
  check.boost.params(x,y,factors)
  
  n <- nrow(x) ## The number of examples
  f <- rep(0,n)  ## The predictions on training set
  margins <- rep(0,n)  ## the margins
  train.err <- c()

  ## The minimum value for weight
  epsilon = min(1e-15, (1 / nrow(x)) / 10^5)
  
  ## Determine the initial potential of each example
  wp <- weight.potential.BrownBoost(margins, params)
  init.p <- wp$p
  cat(sprintf("Initial potential is %0.6f", init.p[1]))
  params$init.p <- init.p
  
  # a list to save model variables
  model = list()
  model$iters <- list()
  model$method <- "brown"
  model[["use.conf"]] <- use.conf
  model[["drift"]] <- params$drift
  model[["get.mu.func"]] <- get.mu
  model[["get.sd.func"]] <- get.sd
  model[["weight.pot.func"]] <- weight.potential.BrownBoost
  model[["init.params"]] <- params


  i<-1
  d <- data.frame(x)
  ## Get weak learners and set weights
  while(i <= max.iters){
    cat('.')
    wp <- weight.potential.BrownBoost(margins, params)
    w <- wp$w
    w <- w/sum(w)

    if (!is.null(factors)) {
      for (j in 1:length(factors)) {
        levels(d[[j]]) <- factors[[j]]
      }
    }

    tree <- base.classifier(x,y,w)
    pred <- predict(tree,d)
    
    err <- sum(1-(y==pred))
    werr <- sum(w*(1-(y==pred)))

    steps <- pred*y

    end.early <- F
    soln <- NULL
    if (err > 0) {
      soln <- solve.constraints(margins, steps, params, verbose)
      if (soln$failed) {
        show(soln)        
        for (asdf in 1:100) { show("Solution has failed to converge while solving constraints"); }
      }
      if (!soln$end.game && 
          (abs(soln$wc.val) > params$constraint.eps ||
           abs(soln$pc.val) > params$constraint.eps)) {
        show(soln)
        for (asdf in 1:100) { show("Solution found does not sufficiently minimize the constraints"); }
      }
      ds <- soln$ds
      dt <- soln$dt
      end.early <- soln$end.game
    } else {
      ds <- Inf
      dt <- params$time.left
      show("Found base hypothesis with no error; using this hypothesis for classification and ending boosting")
    }
    
    
    params$time.left <- params$time.left - dt
    
    f <- f + ds*pred
  
    margins <- 1*f*as.vector(y)
    train.err <- c(train.err, sum(margins<0)/n)

    model[["iters"]][[i]] <- list()
    model[["iters"]][[i]][["tree"]] <- tree
    model[["iters"]][[i]][["alpha"]] <- ds
    model[["iters"]][[i]][["ds"]] <- ds
    model[["iters"]][[i]][["dt"]] <- dt
    ##model[["iters"]][[i]][["scores"]] <- f
    ##model[["iters"]][[i]][["margins"]] <- margins
    ##model[["iters"]][[i]][["preds"]] <- pred
    model[["iters"]][[i]][["soln"]] <- soln
    model[["iters"]][[i]][["params"]] <- params

    #show(f)
    if (verbose) { 
      if (!is.null(soln)) {
        show(sprintf("Iter=%d, train.err=%0.4f, time.left=%0.4f, ds=%0.4f, dt=%0.5f (g=%0.5f, wc=%0.5f, pc=%0.5f, solver=%s)",
                     i, mean(margins<0), params$time.left,        ds,       dt, soln$gamma, soln$wc.val, soln$pc.val, soln$solver) )
      } else {
        show(sprintf("Iter %d, err=%0.4f, time.left=%0.4f, ds=%0.4f, dt=%0.5f",
                     i, mean(margins<0), params$time.left,      ds,      dt) )
      }
    }

    if (end.early==T || params$time.left < params$early.stop) {
      break
    }
  
    i<-i+1
  }
  wp <- weight.potential.BrownBoost(margins, params)
  cat(sprintf("\nFinal potential is %0.6f\n", mean(wp$p)))
  cat(sprintf("Final train error is %0.6f\n", mean(margins < 0)))

  class(model) <- 'boost'
  return (model)
}




## 
## Setup the list of parameters to pass to brownboost.
## You must set margin.target and one of the 'error potential' parameters (noise.estimate, target.pot, or total.time).
## @param margin.target The classification margin targetted (Recommended value between 0.1 and 1).  The higher this value, the greater the margin.  Large values may require considerable time to finish running.  Note: Boosting error bounds are based on the *normalized* margin.  The value that is optimized by this package is the *non-normalized* margin.  There is no known boosting algorithm that directly optimizes normalized margin.
## @param noise.estimate Training error target in [0,1].  This is essentially same parameter as target.pot, but with a friendlier name.
## @param target.pot Target error potential in [0, 1]. This parameter is related to the target "error rate".  To understand the subtle difference, see manuscript.
## @param total.time Total time to play the drifting game, >0.
## 
get.brown.boost.params <- function(margin.target=0.2, noise.estimate=NULL, target.pot=NULL, total.time=NULL) { 
  err.msg <- "Only specify one of noise.estimate, target.pot, and total.time.  These parameters determine one another, so set one and the other will be calculated and returned."
  if ((!is.null(noise.estimate) + !is.null(target.pot) + !is.null(total.time))  > 1) { stop(err.msg); }
  if (!is.null(noise.estimate)) target.pot <- noise.estimate
  if (is.null(total.time))  total.time <- get.time.from.pot(target.pot, margin.target)
  if (total.time < 0.000001) stop("Must set total time to > 0")

  params <- list()
  params$noise.estimate <- noise.estimate
  params$total.time <- total.time
  params$target.pot <- target.pot
  params$time.left <- total.time
  params$drift <- margin.target
  params$eps <- 0.000001
  params$constraint.eps <- 0.003
  params$target.eps.div <- 100
  params$early.stop <- 0.01
  params$change.var <- T

  return(params)
}





############################################################
## The following functions are intended for internal use by
## the driftboost algorithm.  They may be of interest to 
## those who would like to extend the algorithm, but should
## not be used by standard users of the package.
############################################################

## Bisection algorithm to identify total time to for drifting game based on target potential (error rate)
get.time.from.pot <- function(target.pot, drift) {
  if (target.pot > 0.45) { stop("You have specified target loss >0.45.  This means you expect there to be >45% error in a binary classification task.  This may be implemented in assymetric loss function, but for symmetric loss function this is too high.") } 
  right.time <- 10
  left.time <- 0.00001
  params <- list()
  params$drift <- drift
  pot <- -9999999
  i <- 0
  while (abs(pot - target.pot) > 0.00000001) {
    #if (i < 100) show(round(c(i,pot,target.pot), 5))
    #if (i >= 20 && i < 100 && i %% 5 == 0) show(round(c(i,pot,target.pot), 5))
    #if (i >= 100 && i < 1000 && i %% 100 == 0) show(round(c(i,pot,target.pot), 5))
    #if (i >= 1000 && i < 10000 && i %% 1000 == 0) show(round(c(i,pot,target.pot), 5))
    #if (i >= 10000 && i %% 100000 == 0) show(round(c(i,pot,target.pot), 5))
    i <- i + 1
    mid.time <- (right.time + left.time) / 2
    #show(sprintf("mid.time=%0.4f", mid.time))
    params$total.time <- mid.time
    params$time.left <- mid.time
    margin <- c(0)
    wp <- weight.potential.BrownBoost(margin, params)
    #show(sprintf("pot of mid.time = %0.4f", wp$p))
    if (wp$p > target.pot) {
      left.time = mid.time
    } else {
      right.time = mid.time
    }
    pot <- wp$p
  }
  return (mid.time)
}


erf <- function(x) {
  return(2*pnorm(x*sqrt(2)))
}


## calculate the weight and potential of examples at a given margins
weight.potential.BrownBoost <- function(margins, params) {
  mu <- get.mu(params)
  sd <- get.sd(params)
  #show(sprintf("mu=%0.4f, sd=%0.4f", mu, sd))

  p <- pnorm(margins, mean=mu, sd=sd, lower.tail=F)
  w <- dnorm(margins, mean=mu, sd=sd)
  #w <- exp(-(margins-mu)^2/sd^2)
  return (list(w=w, p=p))
}

get.sd <- function(params) {
  #final.sd <- params$final.sd
  #val = params$final.sd - params$total.time * (params$time.left / params$total.time)
  #val = sqrt(params$total.time)
  if (is.null(params$early.stop)) {params$early.stop<-0}
  val = sqrt(params$time.left+params$early.stop)
  return (val)
}

get.mu <- function(params) {
  val = params$drift - params$time.left
  return (val)
}

weight.constraint <- function(margins, steps, ds, dt, params, ret.wp=F) {
  tmp.params <- params
  tmp.params$time.left <- tmp.params$time.left - dt
  tmp.margins <- margins + ds * steps
  wp <- weight.potential.BrownBoost(tmp.margins, tmp.params)
  if (ret.wp) { return (wp) }
  val <- sum(steps * wp$w)
  return (val)
}

potential.constraint <- function(margins, steps, ds, dt, params, ret.wp=F) {
  tmp.params <- params
  tmp.params$time.left <- tmp.params$time.left - dt
  tmp.margins <- margins + ds * steps
  wp <- weight.potential.BrownBoost(tmp.margins, tmp.params)
  if (ret.wp) { return (wp) }
  val <- sum(tmp.params$init.p - wp$p)
  return (val)  
}


# Find step size for both margin and time steps
solve.constraints <- function(margins, step, params, verbose=F) {
  wp <- weight.potential.BrownBoost(margins, params)
  gamma <- mean(step * (wp$w/sum(wp$w)))
  
  init.ds <- 0.5 * log((1+gamma)/(1-gamma))
  newton.time.start <- Sys.time()
  newton.soln <- newton.solver(margins, step, init.ds, params, verbose=verbose)
  newton.time.end <- Sys.time()
  soln <- newton.soln
  #soln <- list()
  #soln$failed <- T
  if (verbose) {
    bisection.time.start <- Sys.time()
    bisection.soln <- bisection.solver(margins, step, init.ds, params)
    bisection.time.end <- Sys.time()
    show(c("Time for newton is", newton.time.end - newton.time.start))
    show(c("Time for bisection is", bisection.time.end - bisection.time.start))
    show("Bisection solution")
    show(bisection.soln)
    show("Newton solution")
    show(newton.soln)
    if (!newton.soln$failed &&
        (abs(bisection.soln$ds - newton.soln$ds) > 0.001 ||
         abs(bisection.soln$dt - newton.soln$dt) > 0.001)) {
      show("Bisection solution")
      show(bisection.soln)
      show("Newton solution")
      show(newton.soln)
      stop("Solns don't match")   
    }
  }

  if (soln$failed ||
      abs(soln$wc.val) > params$constraint.eps ||
      abs(soln$pc.val) > params$constraint.eps) {
    if (verbose)    show("Newton may have failed, now using bisection")
    soln <- bisection.solver(margins, step, init.ds, params)
    if (soln$failed ||
        abs(soln$wc.val) > params$constraint.eps ||
        abs(soln$pc.val) > params$constraint.eps) {
     if (verbose) cat(sprintf("   Newton and bisection solutions did not converge.  This is okay if at end of game with time.left (current parameter set to: %0.3f).  \n", params$early.stop))
    }
  }

  if (soln$dt < 0 && soln$dt > -0.001) {
    soln$dt <- 0
  }

  if (soln$dt < 0) {
    show(soln)
    show("Solution found for dt is less than zero (moving backwards in time)!")
  }
  
  soln$end.game <- F
  if (params$time.left - soln$dt < params$early.stop) {
    soln$end.game <- T 
  }

  soln$gamma <- gamma
  return(soln)
}


#sd.wrt.dt <- function(margins, steps, ds, dt, params) {
#  val <- (get.sd(params) - dt)
#  return(val)
#}

ONE.DIV.2.PI <- 1 / sqrt(2*pi)
exp.wrt.ds <- function(margins, steps, ds, dt, params) {
  tmp.params <- params
  tmp.params$time.left <- params$time.left - dt 
  val <- -2 * (margins + steps*ds - get.mu(tmp.params)) / get.sd(tmp.params)^2
  return (val)
}

wc.wrt.ds <- function(margins, steps, ds, dt, params) {
  wp <- weight.constraint(margins, steps, ds, dt, params, T)
  val <- steps^2 * wp$w * exp.wrt.ds(margins, steps, ds, dt, params)
             #ONE.DIV.2.PI/get.sd(tmp.params))
          
  return(sum(val))
}

exp.wrt.dt <- function(margins, steps, ds, dt, params) {
  tmp.params <- params
  tmp.params$time.left <- params$time.left - dt 
  val <- -2 * (margins + steps*ds - get.mu(tmp.params))^2 / get.sd(tmp.params)^4
  return(val)
}

wc.wrt.dt <- function(margins, steps, ds, dt, params) {
  wp <- weight.constraint(margins, steps, ds, dt, params, T)
  val <- steps * wp$w * exp.wrt.dt(margins, steps, ds, dt, params) 
             #* ONE.DIV.2.PI/get.sd(tmp.params)) +
             #  ((steps * wp$w * exp.wrt.dt(margins, steps, ds, dt, params) 
             #* ONE.DIV.2.PI/get.sd(tmp.params)) )
  return(sum(val))
}

exp.sqrt <- function(margins, steps, ds, dt, params) {
  tmp.params <- params
  tmp.params$time.left <- params$time.left - dt
  val <- (margins + steps * ds - get.mu(params)) / get.sd(params)
  return (val)
}

pc.wrt.ds <- function(margins, steps, ds, dt, params) {
  wp <- weight.constraint(margins, steps, ds, dt, params, T)
  val <- steps * wp$w * (exp.wrt.ds(margins, steps, ds, dt, params)
                          / exp.sqrt(margins, steps, ds, dt, params))
          #* ONE.DIV.2.PI/get.sd(tmp.params))
  return(sum(val))
}

pc.wrt.dt <- function(margins, steps, ds, dt, params) {
  wp <- weight.constraint(margins, steps, ds, dt, params, T)
  val <- wp$w * (exp.wrt.dt(margins, steps, ds, dt, params) 
                  / exp.sqrt(margins, steps, ds, dt, params))
          #* ONE.DIV.2.PI/get.sd(tmp.params))
  return(sum(val))
}

l2dist <- function(x,y) { sqrt(sum((x-y)^2)); }

newton.solver <- function(margins, steps, init.ds, params, max.search.iter=1000, verbose) {
  soln <- list()
  soln$failed <- F
  soln$solver <- "newton"
  soln$end.game <- F
  
  ds <- init.ds
  dt <- min(init.ds^2, params$time.left-0.001)
  delta <- c(ds, dt)
  niter <- 1
  prev.delta <- NA
  while (T) {
    wc.ds <- wc.wrt.ds(margins, steps, ds, dt, params)
    wc.dt <- wc.wrt.dt(margins, steps, ds, dt, params)
    pc.ds <- pc.wrt.ds(margins, steps, ds, dt, params)
    pc.dt <- pc.wrt.dt(margins, steps, ds, dt, params)
    J = matrix(c(wc.ds, wc.dt, pc.ds, pc.dt), ncol=2, byrow=T)
    #show(J)
    #show(ds)
    #show(dt)
    invJ <- try(solve(J),TRUE)
    if (inherits(invJ,"try-error")){
      show("Reason for failure: J is singular!")
      soln$failed <- T
      break
    }

    wc <- weight.constraint(margins, steps, ds, dt, params)
    pc <- potential.constraint(margins, steps, ds, dt, params)
    constraints <- matrix(c(wc, pc), ncol=1)
    
    delta <-  invJ %*% constraints
    #delta <- delta / 2
    ds.delta <- -delta[1]
    dt.delta <- -delta[2]
    
    #show(sprintf("iter %d: wc=%0.5f, pc=%0.5f, ds=%0.5f -> %0.5f, dt=%0.5f -> %0.5f\n", niter, wc, pc, ds, ds + ds.delta, dt, dt + dt.delta))

    if (niter >= max.search.iter) {
      soln$failed <- T
      break
    }
    
    delta.norm <- l2dist(delta,c(0,0))
    if (is.na(delta.norm) ||
        (!is.na(prev.delta) && l2dist(prev.delta, c(0,0)) <= delta.norm)) {
      prev.delta.norm <- l2dist(prev.delta, c(0,0))
      if (verbose) {   show(sprintf("Delta size increased: %0.5f -> %0.5f\n", prev.delta.norm, delta.norm)); }
      soln$failed <- T
      break
    }

    prev.delta <- delta

    wc.val <- weight.constraint(margins, steps, ds, dt, params)
    pc.val <- potential.constraint(margins, steps, ds, dt, params)
    #show(sprintf("iter %d: wc=%.8f, pc=%f, ds=%f, dt=%f", niter, wc.val, pc.val, ds, dt))
    soln$wc.val <- wc.val
    soln$pc.val <- pc.val

    ds <- ds + ds.delta
    dt <- dt + dt.delta

    soln$ds <- ds
    soln$dt <- dt

    # Use arbitrary cutoff to claim the game has ended.
    if (params$time.left - dt <= params$early.stop) {
      ##cat("   Ending the game in newton solver!\n")
      soln$failed <- T
      #soln$end.game <- T
      break
    }

    if (wc.val < params$constraint.eps/params$target.eps.div &&
        pc.val < params$constraint.eps/params$target.eps.div) {
      break
    }

    niter <- niter + 1
  }
  return (soln)
}

bisection.solver <- function(margins, steps, init.ds, params, max.search.iter=100) {
  soln <- list()
  soln$failed = F
  soln$solver <- "bisection"
  #soln$end.game = F

  niter <- 1
  ds = params$eps
  dt = params$eps
  dt.step.size = 0.01

  prev.pc.val <- NA
  direction = 1
  best.pc.val = Inf

  #show(sprintf("Bisection solver:"))
  while (T) {

    # Get initial values for the left and right endpoints of possible ds values
    left.ds = -.00001
    val.left = weight.constraint(margins, steps, left.ds, dt, params)
    while (val.left < 0) {
      left.ds = left.ds * 1.1
      val.left = weight.constraint(margins, steps, left.ds, dt, params)
      #show(sprintf("left.ds=%0.4f, val.left=%0.4f", left.ds, val.left))
    }

    right.ds = 0.3
    val.right = weight.constraint(margins, steps, right.ds, dt, params)
    #show(sprintf("right.ds=%0.4f, val.right=%0.4f", right.ds, val.right))
    while (val.right > 0) {
      right.ds = right.ds * 1.1
      val.right = weight.constraint(margins, steps, right.ds, dt, params)
      #show(sprintf("right.ds=%0.4f, val.right=%0.4f", right.ds, val.right))
    }
    #show(sprintf("right.ds=%0.4f, val.right=%0.4f", right.ds, val.right))
    #show(sprintf("left.ds=%0.4f, val.left=%0.4f", left.ds, val.left))
  
    # Iterate until we converge on a value for ds
    while (abs(val.right - val.left) > params$eps) {
      mid.ds = (left.ds + right.ds) / 2
      val.mid = weight.constraint(margins, steps, mid.ds, dt, params)
      val.left = weight.constraint(margins, steps, left.ds, dt, params)
      val.right = weight.constraint(margins, steps, right.ds, dt, params)
      #show(sprintf("mid.ds=%0.4f, val.mid=%0.4f", mid.ds, val.mid))

      if (sign(val.left) == sign(val.mid)) {
        left.ds = mid.ds
        val.left = val.mid
      } else if (sign(val.right) == sign(val.mid)) {
        right.ds = mid.ds
        val.right = val.mid
      } else {
        show(sprintf("left_ds=%f, val_left=%f, mid_ds=%f, val_mid=%f, right_ds=%f, val_right=%f",
                      left.ds, val.left, mid.ds, val.mid, right.ds, val.right))
        stop("bisection_solver: right_ds and left_ds on same side")
      }
    }
    ds = (left.ds + right.ds) / 2

    #show(c("ds is", ds))
  
    # We have found a reasonable value for ds, now update dt

        
    wc.val <- weight.constraint(margins, steps, ds, dt, params)
    pc.val <- potential.constraint(margins, steps, ds, dt, params)
    #show(sprintf("iter %d: wc=%.8f, pc=%f, ds=%f, dt=%f", niter, wc.val, pc.val, ds, dt))
       
    if (abs(pc.val) < abs(best.pc.val)) {
      best.pc.val <- pc.val
    }

    # Have we found a value of ds such that wc =0 and dt is such that pc=0
    if (abs(pc.val) < params$eps) {
      break
    }
  
    if (niter >= max.search.iter) {
      soln$failed <- T
      break
    }
    
    if (!is.na(prev.pc.val) && (sign(pc.val) != sign(prev.pc.val))) {
      # We're somewhere around pc.val = 0, so take smaller steps
      dt.step.size <- dt.step.size / 2
    } else {
      dt.step.size <- dt.step.size * 1.2
    }
       
    if (pc.val > 0) {
      direction = 1
    } else {
      direction = -1
    }
  
    prev.pc.val = pc.val

    #show(c("dt is", dt, "step.size=",dt.step.size,"direction", direction,"pc.val",pc.val))
    dt <- dt + direction * dt.step.size
    #show(c("updated dt is", dt, "step.size=",dt.step.size,"direction", direction,"pc.val",pc.val))

    #Check end game conditions
    while (params$time.left - dt <= 0) {
      dt.step.size = dt.step.size / 2
      dt = dt - direction * dt.step.size
    }
      
    wc.val <- weight.constraint(margins, steps, ds, dt, params)
    pc.val <- potential.constraint(margins, steps, ds, dt, params)
    #show(sprintf("iter %d: wc=%.8f, pc=%f, ds=%f, dt=%f", niter, wc.val, pc.val, ds, dt))
    soln$wc.val <- wc.val
    soln$pc.val <- pc.val

    # Use arbitrary cutoff to claim the game has ended.
    if (params$time.left - dt <= params$early.stop) {
      ##cat(sprintf("   Ending the game!\n"))
      #soln$end.game <- T
      break
    }

    niter <- niter + 1
  }

  soln$ds <- ds
  soln$dt <- dt

  return (soln)
}


