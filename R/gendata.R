gen.split.cube <- function(n,d,J,q) {
  x<-matrix(0,n,d)
  for (ddd in 1:d){
    x[,ddd]<-runif(n)
  }
  true.y<-rep(0,n)
  y<-rep(0,n)
  for (i in 1:n){
    prob.y.given.x <- (q+(1-2*q)*1*(sum((x[i,1:J]))>(J/2)))
    y[i]<-1*(runif(1)<prob.y.given.x)
    true.y[i]<-1*(q + 0.00001 < prob.y.given.x)
  }
  err = 1-mean(true.y==y)
  
  y <- matrix(2*(y-0.5), ncol=1)
  true.y <- matrix(2*(true.y-0.5), ncol=1)
  x <- as.data.frame(x)
  colnames(x) <- sprintf("d%d",1:d)
  return (list(x=x,y=y,true.y=true.y,err=err,params=list(n=n,d=d,J=J,q=q)))
}


get.long.servedio <- function() {
  gamma <- 0.25
  train.size <- 1000
  x <- matrix(0, train.size, 21)
  y <- matrix(0, train.size, 1)

  if (train.size %% 4 != 0) {
    stop("Size of train set must be divisible by 4")
  }
  
  ## pullers
  j <- 1
  for (i in 1:(train.size/4)) {
    y[j] <- sample(c(-1,1),1)
    x[j,1:11] <- y[j]
    x[j,12:21] <- -y[j]
    ##show(y[j])
    ##show(x[j,])
    j <- j + 1
  }
               
  ## penalizer
  for (i in 1:(train.size/2)) {
    y[j] <- sample(c(-1,1),1)
    random.5.of.1.to.11.idx <- sample(11,5)
    ##show(random.5.of.1.to.11.idx)
    x[j, random.5.of.1.to.11.idx] <- y[j]
    random.6.of.12.to.21.idx <- sample(c(12:21),6)
    ##show(random.6.of.12.to.21.idx)
    x[j, random.6.of.12.to.21.idx] <- y[j]
    x[j, -c(random.5.of.1.to.11.idx,random.6.of.12.to.21.idx)] <- -y[j]
    ##show(-c(random.5.of.1.to.11.idx,random.6.of.12.to.21.idx))
    ##show(y[j])
    ##show(x[j,])
    j <- j + 1
  }
               
  ## large margin
  for (i in 1:(train.size/4)) {
    y[j] <- sample(c(-1,1),1)
    x[j,] <- rep(y[j],21)
    ##show(y[j])
    ##show(x[j,])
    j <- j + 1
  }

  true.y <- y

  flip.idx <- sample(1:train.size, train.size/10)
  #y[flip.idx] <- -y[flip.idx]
  err <- 0.1

  colnames(x) <- sprintf("d%d",1:ncol(x))
  return (list(x=x,y=y,true.y=true.y,err=err,params=list(n=train.size,d=2,q=0.1)))
}



mod2 <- function(int) {
  if (round(int)==int) {
    return(1-1*(round(int/2,0)==int/2))
  } else {
    return("error: this function only works on integers")
  }
}


gen.multisplit.cube <- function(n,d,J,q) {
  x<-matrix(0,n,d)
  for (ddd in 1:d){
    x[,ddd]<-runif(n)
  }

  y<-rep(0,n)
  for (i in 1:n){
    prob.y.given.x <- (q+(1-2*q)*1*mod2(floor(10*x[i,1])))
    y[i]<-1*(runif(1)<prob.y.given.x)
    true.y[i]<-1*(q + 0.00001 < prob.y.given.x)
  }
  y <- matrix(2*(y-0.5), ncol=1)
  
  return (list(x=x,y=y,true.y=true.y,err=err,params=list(n=n,d=d,J=J,q=q)))
}

