### README

This repository contains convex and non-convex boosting algorithms implemented in R.

Author: Aaron Arvey.
Version: 0.3 
Date: Sept 6th 2017

### Install and usage ###

Current usage source('AdaBoost.R') and source('BrownBoost.R').

Examples of code can be found in `code_for_paper.R`.

You can read original manuscript (Arvey, 2010) to better understand the parameters and implementation.


